# Newscred OMDB Api Demo
This Flask project uses OMDB Api to search for movies and render in html.

### How to run
```bash
$ git clone https://github.com/tahmid-choyon/newscred_omdb.git
$ cd newscred_omdb
$ pip install -r requirements.txt
$ python app.py
```

### Misc
Get your OMDB Api key form [here](http://www.omdbapi.com/) , set as an environment variable
```bash
$ export API_KEY=YOUR_API_KEY
$ python app.py
```